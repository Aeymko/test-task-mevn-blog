 Frontend: Vue.js + TailwindCSS / (Vite.js)

 Backend: Node.js / Express.js

 Database: MongoDB
 


 !Important! Change CRLF to LF for client/entrypoint.sh file (I forgot autocrlf false)

 Run:

 cd to server folder and: docker build -t mevn_stack_blog_miroshnychenko_oleksii_server .

 cd to client folder and: docker build -t mevn_stack_blog_miroshnychenko_oleksii_client .

 to main folder: cd ..
 
 compose: docker-compose up

