module.exports = {
  mode: 'jit',
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {
      "gray": "#ffffff",
      "black": "#1e1e1e",
      "blue": "#D7884C",
      "light-blue": "#D7884C",
      "dark-blue": "#1e1e1e",
      "red": "rgb(234 88 12)",
    },
    extend: {
      fontFamily: {
        'nunito': ['"Nunito"', 'sans-serif']
      }
    },
    screens: {
      "xs": "480px"
    }
  },
  plugins: [
    require('tailwind-scrollbar-hide')
  ],
}